import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import ReduxProvider from './components/core/ReduxProvider'
import registerServiceWorker from './registerServiceWorker'

ReactDOM.render(<ReduxProvider />, document.getElementById('root'))
registerServiceWorker()