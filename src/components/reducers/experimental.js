
function makeActionCreator(type, ...argNames) {
  return function (...args) {
    const action = { type }
    argNames.forEach((arg, index) => {
      action[argNames[index]] = args[index]
    })
    return action
  }
}

const inc = makeActionCreator("INC", 'delta')
const dec = makeActionCreator("DEC", 'delta')

export { inc, dec }

/*const actionsMap = {
  increment: change => state => this.state + 1,
  decrement: change => state => this.state - 1
}

function createActions(actions, dispatch) {
  return Object.keys(actions).reduce(
    (accumulator, currentkey) => {
      accumulator[currentkey] = (...args) => {
        return dispatch({
          type: currentkey,
          payload: args
        });
    };
    return accumulator;
  }, {});
}

createActions(actionsMap, {})*/