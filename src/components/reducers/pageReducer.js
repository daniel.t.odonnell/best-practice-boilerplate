const initialState = {}

export default function page(state = initialState, action) {
  switch(action.type) {
    case "PAGE_INIT":
      console.log(action)
      return initialState
    case "GET_PAGE_LIST":
      console.log(action)
      return {...state, ...{ list: action.payload }}
    default:
      return state
  }
}