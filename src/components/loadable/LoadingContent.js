import React, { Component } from 'react';
import logo from '../../logo.svg';


class LoadingContent extends Component {
  render() {
    return (
      <div><img src={logo} className="App-logo" alt="Loading Async Component..." /></div>
    );
  }
}

export default LoadingContent;
