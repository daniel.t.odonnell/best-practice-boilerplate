import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class TestAsyncContent extends Component {

  render() {
    return (
      <div>
        <h1>Async Content Loaded!</h1>
        <Link to='/'>Unload</Link>
      </div>
    );
  }
}

export default TestAsyncContent;
