import React, { Component } from 'react';

import ContentRouter from '../core/ContentRouter'

import logo from '../../logo.svg';
import '../../resources/scss/App.css';

class App extends Component {

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Best Practice Boilerplate</h1>
          <div><small>You are running this application in <b>{process.env.NODE_ENV}</b> mode.</small></div>
        </header>
        <div>
          <ContentRouter />
        </div>
      </div>
    );
  }
}

export default App;
