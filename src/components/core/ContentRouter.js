import React, { Component } from 'react';
import {Switch, Route, Link} from 'react-router-dom';

import PageList from '../wordpress/PageList';
import PageDetail from '../wordpress/PageDetail';

import Loadable from 'react-loadable';
import Loading from '../loadable/LoadingContent';

const Loadable_TestAsync = Loadable({
  loader: () => import('../loadable/TestAsyncContent'),
  loading: Loading,
});

class ContentRouter extends Component {

  render() {
    return (
      <Switch>
        <Route exact path='/'>
          <div>
            <PageList />
            <hr />
            <Link to='async'>Load Async Content...</Link>
          </div>
        </Route>
        <Route path='/async' component={Loadable_TestAsync} />
        <Route path='/page/:id/:title' component={PageDetail} />
      </Switch>
    );
  }
}

export default ContentRouter;
