import React, { Component } from 'react'
import { createStore, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'

import Router from './Router'
import reducerIndex from '../reducers/index'

class ReduxProvider extends Component {

  reduxStore = createStore(
      reducerIndex, 
      /* preloadedState, */
      window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );

  render() {
    return (
      <Provider store={this.reduxStore}>
        <Router />
      </Provider>
    )
  }
}

export default ReduxProvider
