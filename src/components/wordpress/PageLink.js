import React, { Component } from 'react';
import {Link} from 'react-router-dom';


class PageLink extends Component {

  constructor(props) {
    super(props)

    // WP renders titles to encoded html
    this.state = {
      title: {
        __html: props.page.title.rendered
      },
    }
  }

  render() {
    return (
      <div>
        <Link to={`page\\${this.props.page.id}\\${encodeURI(this.props.page.title.rendered)}`}>
          <span dangerouslySetInnerHTML={this.state.title} />
        </Link>
      </div>
    );
  }
}

export default PageLink;
