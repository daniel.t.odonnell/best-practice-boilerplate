import React, { Component } from 'react'
import axios from 'axios'

class PageDetail extends Component {

  constructor(props) {
    super(props)

    this.getPage = this.getPage.bind(this)

    this.state = { 
    }
  }

  componentDidMount() {
    this.loadWPPage()
  }

  componentWillReceiveProps(nextProps) {
    this.props = nextProps;
  }

  async loadWPPage() {
    const pageDetails = await this.getPage()
    console.log("pageDetails", pageDetails)

    //format
    pageDetails.result.title.__html = pageDetails.result.title.rendered
    pageDetails.acf.single_content_dangerous = { __html: pageDetails.acf.single_content }

    this.setState({...this.state, ...{ page: pageDetails }})
  }

  async getPage() {
    let pageId = this.props.match.params.id;
    const result = await axios(`/api/?rest_route=/wp/v2/pages/${pageId}`)
    const resultACF = await axios(`/api/?rest_route=/acf/v3/pages/${pageId}`)
    console.log("result", result)
    console.log("acf", resultACF)
    return { "result": result.data, "acf": resultACF.data.acf }
  }

  render() {
    return (
      <div>
        {this.state && this.state.page && 
          <div>
            <h2 dangerouslySetInnerHTML={this.state.page.result.title}></h2>
            <div dangerouslySetInnerHTML={this.state.page.acf.single_content_dangerous}></div>
          </div>
        }
      </div>
    );
  }
}

export default PageDetail
