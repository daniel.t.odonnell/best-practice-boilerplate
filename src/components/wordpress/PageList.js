import React, { Component } from 'react'
import axios from 'axios'
import { connect } from 'react-redux'

import PageLink from './PageLink'

class PageList extends Component {

  constructor(props) {
    super(props)

    this.getPages = this.getPages.bind(this)
  }

  componentDidMount() {
    this.loadWPPages()
  }

  async loadWPPages() {
    const pages = await this.getPages()
    this.props.dispatch({ type: "GET_PAGE_LIST", payload: pages })
    console.log(this.state)
  }

  async getPages() {
    const result = await axios("/api/?rest_route=/wp/v2/pages")
    console.table(result)
    return result.data
  }

  render() {
    return (
      <div>
        <h2>Pages</h2>
        {this.props.pages && this.props.pages.map( item => 
          <PageLink page={item} key={item.id} /> 
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pages: state.page.list
  }
}


export default connect(mapStateToProps)(PageList)
